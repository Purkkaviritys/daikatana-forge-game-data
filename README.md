# Daikatana Forge Game Data files for Trenchbroom

Conversion of Daikatana's ION Radiant QuakEd definition (.def) files into Trenchbroom/Hammer compatible Forge Game Data (.fgd) files. Project has been structured followingly. The *bin* has a Python utility skeleton for helping out conversion from .def into .fgd probably helpful in other games using the format. Then in *dist* directory you'll be able to find the most current version of the file. And *docs* will have some documentation on things, templates and reference files. From *ext* you can find helpful Visual Studio Code extension for proper highlighting in .fdg files. Also *shared* has a version of the Daikatana fgd which has been split between different files. Finally *tmp* has some files which don't really belong to anywhere, will likely be removed in future but are needed at the moment.

## Known Issues:

* Decorations for all episodes haven't been written into a files yet because there are simply too many of them. Users can use deco_e1, deco_e2, deco_e3, deco_e4 and deco_custom for adding decorations if they wish. Usable decorations for each episode can be seen in models directory after uncompressing PAK files.
* Following trigger entities break the script because the regular expression doesn't consider this particular scenario where all opening characters are capitalised:
   1. *NPCtrigger_waithere*
   1. *NPCtrigger_stopwaithere*
   1. *NPCtrigger_teleport*
   1. *NPCteleport_dest*

* Script doesn't provide verbose error messages. (Other than which particular number of entity on the processing list doesn't process.)
* Script doesn't stop if parsing fails.
* Verifying the parsed scripts hasn't been implemented and many other intended *quality of life* improvements aren't there.
* Multi-line long comments containing usage explanations have been cut out from the entitites in FGD files because Trenchbroom doesn't support them at this time.
