#!/usr/bin/python3
# -*- coding: utf-8 -*-

""" Utility to aid converting Radiant .def to Hammer .fgd format.

This allows people to partially automate conversion of Quaked definition
files used by popular Radiant family of map editors into Valve's Hammer and
Trenchbroom compatible Forge Game Data format files.

TODO: Make it parse Daikatana's QuakeEd .def file and output .fgd file. DONE!
TODO: Practice Python skills. PARTIAL!
TODO: Check that I didn't fuck up anything when writing Daikatana's FGD.
"""

import io
import os
import re
import string
import sys
import time
import getopt

__author__ = "Dekonega"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "dekonega(at)windowslive.com"
__status__ = "Development"


def parse_file(deffile):
    # startTime = time.time()

    # Create a list for entities.
    entities = list()
    values = list()
    fgdfile1 = list()
    index = 1

    # Generate power of two number list for spawnflags.
    flagNumbers = [1 << exponent for exponent in range(15)]

    try:
        fp = open(deffile, "r")
        content1 = fp.read()
    finally:
        fp.close()

    # Prepaire search clause for all C-style comments.
    regex1 = re.compile(r'/\*(?:.|[\r\n])*?\*/')

    # Search clause for relevant information from each comment.
    regex2 = re.compile(r'(\/*\WQUAKED)+\s+((?:[a-z][a-z0-9_]*))+\s+\((.*?)\)+\s+(\?|\((.*?)\)\s\((.*?)\))+(?:\n|\s+((?:[_a-zA-Z0-9].*))+\n)+((?:.|[\r\n])*?)\*\/')

    # Clause for "modelname", "sndkeys", "episode", "spritetype" key's values.
    regex3 = re.compile(r'(?:\"modelname\")+\s+"((?:.*))"')
    regex4 = re.compile(r'(?:\"sndkeys\")+\s+"((?:.*))"')
    regex5 = re.compile(r'(?:\"episode\")+\s+"((?:.*))"')
    regex6 = re.compile(r'(?:\"spritetype\")+\s+"((?:.*))"')

    # Find all C-style comments.
    results = regex1.findall(content1)

    # Iterate through the results in a smart way.
    for result in results:
        item = regex2.findall(result)
        index = index + 1

        try:
            entities.append(item[0])        
        except IndexError:
            # TODO Provide more information about entities which failed get parsed.
            print("Error: Couldn't parse an entity.")
            print(index)
            pass
        continue

    # Formulate the FGD file.
    for entity in entities:
        description = str(entity[7])
        modelname = regex3.findall(description)
        sndkeys = regex4.findall(description)
        episode = regex5.findall(description)
        spritetype = regex6.findall(description)

        if entity[3] == '?':
            fgdfile1.append('@SolidClass ' +
                            'base(AppearFlags) ' +
                            'color(' + entity[2] + ') ' +
                            '= ' + entity[1] + ' : ' +
                            '\"' + entity[1] + '\"' +  # '+\n'
                            # + '\"' + str(entity[7]).rstrip('\n') + '\" ' +
                            '\n[\n')

            # TODO Move this into a function.
            if entity[6] != '':
                fgdfile1.append('\tspawnflags(flags) =\n' + '\t[')

                # if entity[6] != '':
                spawnflags = str(entity[6]).split()
                i = 0

                for spawnflag in spawnflags:
                    fgdfile1.append(f'\n\t\t{flagNumbers[i]} : \"{spawnflag}\" : 0')
                    i = i + 1

                fgdfile1.append('\n\t]')

            fgdfile1.append('\n')
            fgdfile1.append(']')
            fgdfile1.append('\n\n')

        else:
            fgdfile1.append('@PointClass ' +
                            'base(AppearFlags) ' +
                            'color(' + entity[2] + ') ' +
                            'size(' + entity[4] + ', ' + entity[5] + ') ')

            if modelname:
                # TODO If model string ends with .sp2 then don't add .dkm.
                fgdfile1.append('model({ \"path\": \"' + str(modelname[0]) + '.dkm' + '\" }) ')

            fgdfile1.append('= ' + entity[1] + ' : ' +
                            '\"' + entity[1] + '\"' +  # '+\n'
                            # + '\"' + str(entity[7]).rstrip('\n') + '\" ' +
                            '\n[\n')

            # TODO Move this into a function.
            if entity[6] != '':
                fgdfile1.append('\tspawnflags(flags) =\n' + '\t[')

                if entity[6] != '':
                    spawnflags = str(entity[6]).split()
                    i = 0

                    for spawnflag in spawnflags:
                        fgdfile1.append(f'\n\t\t{flagNumbers[i]} : \"{spawnflag}\" : 0')
                        i = i + 1

                fgdfile1.append('\n\t]')

            fgdfile1.append('\n')
            fgdfile1.append(']')
            fgdfile1.append('\n\n')

    # Writing this crap to a file
    try:
        fp = open("test.fgd", "w+", encoding='utf-8')
        i = 0

        for fgd in fgdfile1:
            fp.write(str(fgdfile1[i]))
            i = i + 1

    finally:
        fp.close()

    # endTime = time.time()

    print("All done!")

    sys.exit()


def emit_help():
    print('''
    Usage: Parser.py [OPTION]

    Convert QuakED definition files to Trenchbroom/Hammer FGD files.

    -b, --baseclass\tAdds @BaseClass templates
    -i, --deffile\tQuakED file to process
    -v, --version\tShow version information
    -h, --help\tThis help text

    ''')


def set_baseclass():
    print("TODO: Add BaseClass templates from a separate file.")


def main(argv):

    if len(sys.argv) < 2:
        print("Error: No arguments given!")
        emit_help()
        sys.exit()

    try:
        opts, args = getopt.getopt(argv, "bhvi:", ["baseclass", "help",
                                   "version", "deffile="])
    except getopt.GetoptError:
        emit_help()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-b", "--baseclass"):
            set_baseclass()
        elif opt in ("-h", "--help"):
            emit_help()
            sys.exit()
        elif opt in ("-v", "--version"):
            print("Version: " + __version__)
            sys.exit()
        elif opt in ("-i", "--deffile"):
            deffile = arg
            print('Begun processing QuakED file \"' + deffile + '\"')
            parse_file(deffile)
        else:
            print("Error: Unhandled option!\n")
            sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
